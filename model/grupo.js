var database = require('./database');
var grupo = {};
grupo.selectAll = function(callback){
    database.query("SELECT * FROM Grupo",
    function(error, result) {
        if(error) {
            throw error;
        } else {
            callback(null, result);
        }
    });
}

grupo.select = function(idGrupo, callback) {
    if(database) {
        var consulta = "SELECT * FROM Grupo WHERE idGrupo = ?";
        database.query(consulta, idGrupo, function(error, resultado){
            if(error) {
                throw error;
            } else {
                callback(null, resultado);
            }
        })
    }
}

grupo.insert = function(data, callback) {
    if(database){
        database.query("CALL agregar_grupo(?)", [data.descripcion], function(error, resultado){
            if(error){
                throw error;
            } else {
                callback(null, {"insertId" : resultadId});
            }
        });
    }
}

module.exports = grupo;
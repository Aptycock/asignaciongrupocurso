var database = require('./database');
var curso = {};
curso.selectAll = function(callback) {
    if(database) {
        database.query("SELECT * FROM Curso",
        function(error, result) {
            if(error) {
                throw error;
            } else {
                callback(null, result);
            }
        });
    }
}

curso.select = function(idCurso, callback) {
    if(database) {
        var consulta = "CALL SP_FiltroCursos(?)";
        database.query(consulta, idCurso, function(error, resultado){
            if(error) {
                throw error;
            } else {
                callback(null, resultado);
            }
        })
    }
}

curso.insert = function(data, callback) {
    console.log(data);
    if(database){
        database.query("CALL SP_InsertarCurso(?)", [data.descripcion], function(error, resultado){
            if(error){
                throw error;
            } else {
                callback(null, {"insertId" : resultado.resultadoId});
            }
        });
    }
}

module.exports = curso;
DROP DATABASE AsignacionHorario;
CREATE DATABASE AsignacionHorario;
USE AsignacionHorario;

CREATE TABLE Curso(
	idCurso INT AUTO_INCREMENT NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    PRIMARY KEY(idCurso)
);

CREATE TABLE Grupo(
	idGrupo INT AUTO_INCREMENT NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    PRIMARY KEY(idGrupo)
);


CREATE TABLE Asignacion(
	idAsignacion INT AUTO_INCREMENT NOT NULL,
    idCurso INT NOT NULL,
    idGrupo INT NOT NULL,
    PRIMARY KEY(idAsignacion),
    FOREIGN KEY(idCurso) REFERENCES Curso(idCurso),
    FOREIGN KEY(idGrupo) REFERENCES Grupo(idGrupo)
);

-- PROCEDIMIENTOS ALMACENADOS

-- Agregar Grupo
DELIMITER //
CREATE PROCEDURE SP_InsertarGrupo(in d VARCHAR(255))
BEGIN
	IF NOT EXISTS (SELECT * FROM Grupo WHERE descripcion = d) THEN
	INSERT INTO Grupo (descripcion) VALUES(d);	    
    END IF; 
END //
DELIMITER ;

-- Agregar Curso
DELIMITER //
CREATE PROCEDURE SP_InsertarCurso(in d VARCHAR(255))
BEGIN
	IF NOT EXISTS (SELECT * FROM Curso WHERE descripcion = d) THEN
	INSERT INTO curso (descripcion) VALUES(d);
    END IF;
END //
DELIMITER ;

-- Agregar Asignacion
DELIMITER //
CREATE PROCEDURE SP_InsertarAsignacion(in idG int, in idC int)
BEGIN
	INSERT INTO Asignacion (idCurso, idGrupo) VALUES(idC, idG);
END //
DELIMITER ;

-- Vistas
DROP procedure IF EXISTS `SP_FiltroCursos`;
DELIMITER //
CREATE PROCEDURE SP_FiltroCursos(in x VARCHAR(255))
BEGIN
	SELECT Curso.descripcion FROM Asignacion INNER JOIN Grupo on Asignacion.idGrupo = Grupo.idGrupo 
    INNER JOIN Curso on Asignacion.idCurso = curso.idCurso
    WHERE Curso.descripcion = x;
END //
DELIMITER ;

DROP procedure IF EXISTS `SP_FiltroGrupos`;
DELIMITER //
CREATE PROCEDURE SP_FiltroGrupos(in x VARCHAR(255))
BEGIN
	SELECT Grupo.descripcion FROM Asignacion INNER JOIN Grupo on Asignacion.idGrupo = grupo.idGrupo 
    INNER JOIN Curso on Asignacion.idCurso = curso.idCurso 
    WHERE grupo.descripcion = x;
END //
DELIMITER ;

-- CALLS

CALL SP_InsertarGrupo("PE6DM");
CALL SP_InsertarGrupo("BA5CV");
CALL SP_InsertarGrupo("BA4BM");
CALL SP_InsertarGrupo("PE6AV");

CALL SP_InsertarCurso("Matem�tica");
CALL SP_InsertarCurso("Idioma Ingles");
CALL SP_InsertarCurso("Qu�mica");
CALL SP_InsertarCurso("F�sica");

CALL SP_InsertarAsignacion(1, 4);
CALL SP_InsertarAsignacion(2, 3);
CALL SP_InsertarAsignacion(3, 2);
CALL SP_InsertarAsignacion(4, 1);

CALL SP_FiltroCursos("Matem�tica");
CALL SP_FiltroGrupos("PE6AV");


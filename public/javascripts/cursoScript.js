var cursoScript = function() {
    var main = this;
    var cursoUri = "http://localhost:3000/api/curso/";
    main.curso = ko.observableArray([]);
    main.error = ko.observable();
    main.cursoCargado = ko.observable();
    main.cursoNuevo = {
      descripcion: ko.observable()
    }



    main.agregarCurso = function() {
        var curso = {
            descripcion: main.cursoNuevo.descripcion()
        }
        ajaxHelper(cursoUri, 'POST', curso).done(function (data) {
            main.obtenerCurso();
        });
        $('#mf')[0].reset();
    }

    main.obtenerCurso = function() {
        ajaxHelper(cursoUri, 'GET').done(function(data) {
            main.curso(data);
        });
    }

    function ajaxHelper(uri, method, data) {
        return $.ajax({
            url : uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown){
            main.error(errorThrown);
        
        });
    }
    
    main.obtenerCurso();





     var main = this;
    var grupoUri = "http://localhost:3000/api/grupo/";
    main.grupo = ko.observableArray([]);
    main.error = ko.observable();
    main.grupoCargado = ko.observable();
    main.grupoNuevo = {
      descripcion: ko.observable()
    }

    
    main.agregarGrupo = function() {
        var grupo = {
            descripcion: main.grupoNuevo.descripcion()
        }
        console.log(grupo);
        ajaxHelper(grupoUri, 'POST', grupo).done(function (data) {
            main.obtenerGrupo();
        });
    }

    main.obtenerGrupo = function() {
        ajaxHelper(grupoUri, 'GET').done(function(data) {
            main.grupo(data);
        });
    }

    function ajaxHelper(uri, method, data) {
        return $.ajax({
            url : uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown){
            main.error(errorThrown);
        
        });
    }
    
    main.obtenerGrupo();
}



$(document).ready(function() {
  var curso = new cursoScript();
  ko.applyBindings(curso);
});
var express = require('express');
var categoria = require('../model/curso');
var router = express.Router();

router.get('/api/curso/', function(req, res) {
    categoria.selectAll(function(error, result){
        if (typeof result !== undefined) {
            res.json(result);
        } else {
            res.json({"Mensaje" : "No hay categorias"});
        }
    });
});

router.post('/api/curso', 
    function(req, res) {
        var data = {
            descripcion: req.body.descripcion
        }
        categoria.insert(data, function(error, rows){
            if(typeof(rows) !== undefined){
                res.json(rows)
            } else {
                res.json({'Mensaje' : 'Error'})
            }
        });
});




module.exports = router;